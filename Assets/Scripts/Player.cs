﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Player : MonoBehaviour
{
	PanoramicVideo panoV;

	public GameObject panoramicVideo;

	float rotX;
	float rotY;
	const float mouseSensitivity = 100.0f;
	const float rotXClamp = 90.0f;

	void Start ()
	{
		panoV = panoramicVideo.GetComponent<PanoramicVideo>();

		Vector3 rot = transform.localRotation.eulerAngles;
		rotX = rot.x;
		rotY = rot.y;
	}

	void Update ()
	{
		if (Input.GetKey(KeyCode.A)) {
			transform.Rotate(new Vector3(0, -1, 0));
		} else if (Input.GetKey(KeyCode.D)) {
			transform.Rotate(new Vector3(0, 1, 0));
		}

		Screen.lockCursor = true;

		float mX = Input.GetAxis("Mouse X");
		float mY = -Input.GetAxis("Mouse Y");
		rotX += mY * Time.deltaTime * mouseSensitivity;
		rotY += mX * Time.deltaTime * mouseSensitivity;

		rotX = Mathf.Clamp(rotX, -rotXClamp, rotXClamp);

		transform.localRotation = Quaternion.Euler(rotX, rotY, 0.0f);


		if (Input.GetKey(KeyCode.W)) {
			panoV.Play(1);
		} else if (Input.GetKey(KeyCode.S)) {
			panoV.Play(0);
		} else {
			panoV.Pause();
		}
	}
}
