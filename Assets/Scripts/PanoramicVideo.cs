﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PanoramicVideo : MonoBehaviour
{
	VideoPlayer vPlayer;

	int currDir = 0;

	void Start ()
	{
		vPlayer = GetComponent<VideoPlayer>();
		vPlayer.Prepare();
	}

	public void Play (int dir)
	{
		if (!vPlayer.isPrepared) {
			return;
		}

		if (dir == 0) {
			if (vPlayer.time > vPlayer.clip.length / 2) {
				vPlayer.time = vPlayer.clip.length - vPlayer.time;
			}

			if (vPlayer.time < vPlayer.clip.length / 2 - 0.05) {
				vPlayer.Play();
			} else {
				vPlayer.Pause();
			}

		} else if (dir == 1) {
			if (vPlayer.time < vPlayer.clip.length / 2) {
				vPlayer.time = vPlayer.clip.length - vPlayer.time;
			}

			if (vPlayer.time < vPlayer.clip.length - 0.05) {
				vPlayer.Play();
			} else {
				vPlayer.Pause();
			}
		}
	}

	public void OnGUI ()
	{
		GUILayout.Label("Time: " + vPlayer.time.ToString());
		GUILayout.Label("Length: " + vPlayer.clip.length.ToString());
	}

	public void Pause ()
	{
		if (vPlayer.isPrepared) {
			vPlayer.Pause();
		}
	}
}
